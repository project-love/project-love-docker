# Project LÖVE Docker Image

This is a Docker image that can be used as a build environment for LÖVE games.

## Usage

To release a LÖVE game using `love-release`, invoke:

    docker run --rm --tty --interactive \
               --volume=`pwd`:/workspace --workdir=/workspace --env="HOME=\workspace" \
               --user=`id --user`:`id --group` \
               registry.gitlab.com/jdmarble/project-love-docker:<tag> love-release <params>
