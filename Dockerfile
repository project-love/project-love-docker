FROM ubuntu:16.04

MAINTAINER James D. Marble <james.d.marble@gmail.com>

# Package purposes:
#
# * ca-certificates - so we can clone from GitHub
# * curl - for downloading luarocks
# * gcc - for building rocks
# * git - for checking out rocks
# * fakeroot and dpkg - required to create Debian packages
# * lua5.1 liblua5.1-0-dev - building luarocks
# * make - projects built with a Makefile and luarocks
# * python2.7 - packaging love.js file systems
# * zip and friends - creating .love files

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
      ca-certificates \
      curl \
      gcc \
      git \
      fakeroot dpkg \
      lua5.1 liblua5.1-0-dev \
      make \
      python2.7 \
      zip unzip libzip-dev && \
    apt-get clean

# Build Luarocks
ENV LUAROCKS_VERSION 2.3.0
ENV LUAROCKS_INSTALL luarocks-$LUAROCKS_VERSION
RUN curl -O http://keplerproject.github.io/luarocks/releases/$LUAROCKS_INSTALL.tar.gz && \
    tar xvzf $LUAROCKS_INSTALL.tar.gz && \
    rm $LUAROCKS_INSTALL.tar.gz && \
    cd $LUAROCKS_INSTALL && \
    ./configure && make build && make install && \
    cd ..

# Install love-release
RUN luarocks install --server=http://luarocks.org/dev lua-zip ZIP_LIBDIR=/usr/lib/x86_64-linux-gnu && \
    luarocks install love-release

# Install love.js
RUN git clone https://github.com/TannerRogalsky/love.js.git && \
    cd love.js && \
    git submodule update --init --recursive && \
    cd ..

